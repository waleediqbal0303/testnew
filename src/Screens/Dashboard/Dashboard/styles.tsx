import { StyleSheet, Platform } from 'react-native';
import {color, baseStyles} from '../../../res';

export default StyleSheet.create({

    main_view:{
    },
    curve_view:{
        width: '100%',
        height: 80,
        backgroundColor: color.Primery,
        borderBottomRightRadius: 30,
        borderBottomLeftRadius: 30,
        transform: [{ scaleX: 1.06 }, { scaleY: 1 }],
    },
    curve_view_inner_view:{
        backgroundColor:'white',marginHorizontal:20,marginTop:-40,borderRadius:12,  borderColor:'#EFEFEF',
        flexDirection:'column',
        alignItems:'center',
        ...Platform.select({
            ios: {
                shadowColor: '#00000029',
                shadowOffset: { width: 2, height: 2},
                shadowOpacity: 1,
                shadowRadius: 0,
                elevation: 20,
            },
            android: {
                shadowColor: '#00000029',
                shadowOffset: { width: 2, height: 2},
                shadowOpacity: 1,
                shadowRadius: 0,
                elevation: 5,
            }
        }),
    },
    curve_view_inner_view_text:{
        padding:10,fontSize:22

    },
    listing_item:{
        backgroundColor:'white',marginHorizontal:20,marginTop:30,borderRadius:12,  borderColor:'#EFEFEF',
        flexDirection:'column',
        alignItems:'center',
        ...Platform.select({
            ios: {
                shadowColor: '#00000029',
                shadowOffset: { width: 2, height: 2},
                shadowOpacity: 1,
                shadowRadius: 0,
                elevation: 20,
            },
            android: {
                shadowColor: '#00000029',
                shadowOffset: { width: 2, height: 2},
                shadowOpacity: 1,
                shadowRadius: 0,
                elevation: 5,
            }
        }),
    },

    





    textfont:{
        fontSize:18,
        fontWeight:'bold'
        

    },
    textfontNormal:{
        fontSize:14,
        

    },
textfontNormal_location:{
        fontSize:14,
        width:70

    },   
    textfontNormal_location1:{
        fontSize:14,
        width:70,
        fontWeight:'bold'

    },  
     textprice_view:{
        alignSelf:'flex-end',
        fontWeight:'100',
        fontSize:14

    },
    textprice_view2:{
        alignSelf:'flex-end',
        color:color.Primery,
        
    fontWeight:'700',
        
        
            

    },
    list2_cart:{
        fontWeight:'700',
        fontSize:18,
     
    },
 
    list4:{
   
        fontWeight:'700',
        fontSize:16,
       color:color.Primery
      
     
    },
    Cartdata:{
        marginRight:0,fontSize:12, marginTop:-15, marginBottom:15,
    },
    innerivew_text:{
        flexDirection:'row',
        marginTop:2,
        // justfyContent:'center',
        alignItems:'center'

    },
    outerview_text:{
        flexDirection:'row'
    },

    mainview_string_cartdata:{
        
  
        justifyContent:'space-between',


    



    },
   
    returnview:{
        flex:1, 

         backgroundColor:color.backgroundScreen,

    },
    
middleView2:{
  flexDirection:'row',
  alignItems:'center',
 
  borderRadius:12,
  marginHorizontal:25,
  marginTop:20,

  

},

    
middleView3_3rd:{
   
    borderTopRightRadius:12,
    borderBottomRightRadius:12,
    padding:10,
    width:'50%',
    backgroundColor:'white',
    borderColor:'#EFEFEF',
  flexDirection:'column',
  alignItems:'center',
 
 


},

middleView3:{
   
    borderTopLeftRadius:12,
    borderBottomLeftRadius:12,
    padding:10,
    width:'50%',
    backgroundColor:'white',
    borderColor:'#EFEFEF',
  flexDirection:'column',
  alignItems:'center',
 
 


},
middleView4:{
    borderRadius:12,
    
    padding:10,
    width:'50%',
    backgroundColor:color.Primery,
    color:'white',
    borderColor:'#EFEFEF',
  
    ...Platform.select({
        ios: {
            shadowColor: '#00000029',
            shadowOffset: { width: 2, height: 2},
            shadowOpacity: 1,
            shadowRadius: 0,
            elevation: 20,
        },
        android: {
            shadowColor: '#00000029',
            shadowOffset: { width: 2, height: 2},
            shadowOpacity: 1,
            shadowRadius: 0,
            elevation: 5,
        }
      }),
   
   
  
     
  flexDirection:'column',
  alignItems:'center',
 


},
list3:{
    borderRadius:12,
    color:'white',
    fontSize:12,
   
    fontWeight:'400',
 
 
},
list2:{
    borderRadius:12,
    fontSize:10,
    fontWeight:'400',
    color:'black',
   
  
 
},
list32:{
    borderRadius:12,
    fontSize:7,
    fontWeight:'700',
    color:color.Primery

},
list222:{
    borderRadius:12,
    fontSize:10,
    fontWeight:'400',
    color:'black',
 
},
list22:{
    borderRadius:12,
    fontSize:8,
    fontWeight:'400',
    color:'black',
    marginTop:5,
    marginHorizontal:5,
    
 
   
  
 
},
list3_icon:{
  
    color:'white',
  
   
    fontWeight:'400',
 
 
},
list2_icon:{
  
   
    fontWeight:'400',
    color:'black',
   
  
 

}
,


addtocartbutton1:{
    backgroundColor:color.Primery,
    padding:10,
    borderRadius:12  ,
    marginHorizontal:0,
    alignSelf:'flex-end',
  
},







addtocartbutton:{
    backgroundColor:color.Primery,
    padding:10,
    marginBottom:10,
    borderRadius:12  ,
    marginHorizontal:20,
    alignSelf:'flex-end',
    marginTop:-30
},
buttontext:{
    justifyContent:'center',
    color:'white',
    alignSelf:'center',
    fontSize:12
},


renderitem_mainview:{
    marginTop:5,
    
   

},
renderinnerview:{
    marginHorizontal:10,
   
},
renderinnerview2:{
    flexDirection:'column',marginRight:0,justifyContent:'space-between'
   
},
flatliststyle:{
    marginBottom:5,

},
render_mainview:{
   backgroundColor:color.backgroundScreen
   

},
cardtouchableopacitylist1:{


    ...Platform.select({
    ios: {
    shadowColor: '#00000029',
    shadowOffset: { width: -2, height: -2},

    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 10,
    },
    android: {
    shadowColor: '#00000029',
    shadowOffset: { width: -5, height: -5},

    shadowRadius: 0,
    elevation: 20,
    shadowOpacity: 0.4,
    }
    }),
    backgroundColor:'white',
    marginLeft:-15,
    marginRight:-10,
    marginTop:22,
    marginBottom:0,
    padding:5,
    borderRadius:5,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:'center',
    zIndex:1
},
cardtouchableopacitylist:{


        ...Platform.select({
        ios: {
        shadowColor: '#00000029',
        shadowOffset: { width: -2, height: -2},

        shadowOpacity: 1,
        shadowRadius: 0,
        elevation: 10,
        },
        android: {
        shadowColor: '#00000029',
        shadowOffset: { width: -5, height: -5},

        shadowRadius: 0,
        elevation: 20,
        shadowOpacity: 0.4,
        }
        }),
        backgroundColor:'white',
        marginBottom:0,
        padding:5,
        borderRadius:5,
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'center',
        zIndex:1
},
Restitemimage:{
        ...Platform.select({
        ios: {
        elevation: 0,
        },
        android: {
        shadowColor: '#00000029',
        shadowOffset: { width: 1, height: 2},
        shadowOpacity: 1,
        shadowRadius: 0,
        elevation: 10,
        }
        }),
        width:0,
        height:0,
        marginRight:0,
        borderRadius:10,
        alignItems:'center',
        justifyContent:'center',
        alignSelf:'center',
        zIndex:1,
},
ratingitem:{
        flexDirection:'row', alignItems:'center', marginTop:5,
        },
styleitem:{
        flex:1,flexDirection:'row',padding:3
},
topviewitem:{

        marginTop:0, marginHorizontal:12,
        ...Platform.select({
        ios: {
        elevation: 0,
        },
        android: {
        shadowColor: '#00000029',
        shadowOffset: { width: 50, height: 30},
        shadowOpacity: 1,
        shadowRadius: 0,
        elevation: 30,
        }
        }),


},


Title:{
color:'#CA373C',
justifyContent:'center',
fontSize:20,
flexDirection:'row',
marginLeft:15,
...baseStyles.fontType

},
titleview:{
flex:1,
marginTop:20,
flexDirection:'row',
alignItems:'center'
},
labelbutton:{
backgroundColor:'#F0F0F0',
borderRadius:12,
padding:0,
margin:3,

...baseStyles.fontType

},

text:{
color:'#414141',
fontSize:12,
justifyContent:'center',
...baseStyles.fontTypethin
},
pirce:{
color: '#CB0909',
fontWeight:'bold',
...baseStyles.fontTypethin

} ,
priceVeiw:{

alignSelf:'flex-end',marginRight:30,

},
priceVeiw2:{

    alignSelf:'flex-end',marginRight:30,flexDirection:'row'
    
    },
textview:{
textAlign:'center',
fontSize:18,
marginLeft:-50,
marginTop:15,

...baseStyles.fontTypethin



},
topbutton:{
padding:4,
marginTop:20,
marginLeft:15,

},
list:{
borderRadius:12,
borderBottomEndRadius:12,
marginTop:0,
fontWeight:'700',

},
list1:{
    borderRadius:12,
    borderBottomEndRadius:12,
    width:165,
    fontWeight:'bold',
    
    },
Image:{

alignSelf:'center',
height:40,
width:40,
marginLeft:20,
borderRadius:12
},
middleView:{
                    borderRadius:12,
                    justifyContent:"center",
                    paddingLeft:20,
                 
                 

                    borderColor:'#EFEFEF',
                    width:'100%',
                    ...Platform.select({
                    ios: {
                    shadowColor: '#00000029',
                    shadowOffset: { width: 2, height: 2},
                    shadowOpacity: 1,
                    shadowRadius: 0,
                    elevation: 20,
                    },

                 
android: {
shadowColor: '#00000029',
shadowOffset: { width: 2, height: 2},
shadowOpacity: 1,
shadowRadius: 0,
elevation: 7,
}
}),


backgroundColor:'white',
borderBottomEndRadius:12

},
middleinnerview:{
    borderRadius:12,
    justifyContent:"center",
    paddingLeft:0,
    flexDirection:'column',
    borderColor:'#EFEFEF',
    width:'100%',
    backgroundColor:'white',
    borderBottomEndRadius:12
},


topOuterView:{
    height:200,
    backgroundColor:color.Primery, 
    justifyContent:"center", 
    alignItems:"center",
    
},
textHeader:{
    color:'#fff',
    fontSize:12
},
textHeaderLeft:{
    color:'#fff',
    fontSize:12, 
    marginTop:10
},
innerTxt:{
    color:'#fff',
    fontSize:14, 
    marginTop:10,
    fontWeight:"bold"
},
mgLft:{marginLeft:10},
status:{
    fontSize:12,
    color:'#fff',
},

statusWorth:{
    fontSize:12,
    color:'#58D68D',
    marginLeft:20,
    fontWeight:"bold"
},

textCounter:{
    fontSize:20,
    color:'#fff',
    fontWeight:'bold'
},
head:{
    flexDirection:"row", 
    alignItems:"center", 
    marginTop:-30
},
view1:{
    flexDirection:"row", height:45, width:200, justifyContent:"center",
    backgroundColor:"rgba(255,255,255,0.2)",
    paddingHorizontal:20,
    alignItems:"center",
    borderRadius:30,
    marginTop:20,
    marginLeft:20
    
    },
view2:{
    width:40, 
    height:40, 
    justifyContent:"center", 
    alignItems:"center", 
    borderRadius:40,
    backgroundColor:"#D4AC0E", 
    marginLeft:240, 
    marginTop:-60
},



});