import React, { Component} from "react";
import { View, Image, ImageBackground, StatusBar, Text ,FlatList,TouchableOpacity,Alert} from "react-native";

import {Header,Loader} from '../../../Components';
import { color } from "../../../res";
import styles from "./styles";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import { connect } from 'react-redux';
import * as Actions from '../../../Actions/Types/ActionTypes';
import {get_Messages} from '../../../Actions/Messages';
import { ScrollView } from "react-native-gesture-handler";
import ScrollableTabView , { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import Recent from './Recent';
import Pending from './Pending';

class Dashboard extends Component {
 
  constructor(props) {
    super(props);
    this.state ={
        pressed: false,
        selected:0,
        fetchingCurrentOrder:false,
        fetchingHistroy:false,
        isFetching:false,
        onEndReachedCalledDuringMomentum:false
    };
    this.extraParams();
  }

  onEndReached = ({ distanceFromEnd }) => {
    if(!this.state.onEndReachedCalledDuringMomentum){
        this.extraParams();
        this.setState({onEndReachedCalledDuringMomentum : true});
    }
  }

  extraParams=async()=>{
    this.props.get_Messages();
    this.setState({isFetching:false});
  }

  _refresh=async()=>{
        this.props.resetPage();
        this.setState({isFetching:true});
        let _this= this;
        setTimeout(function () {
            _this.extraParams(); 
        }, 500);
    }

  




    showAlert(text){
        alert(text);
    }

    render() {

      let messages= this.props.messages;
		return (
			<View style={{flex:1}}>
                <Header screen={"DashBoard"}  
                    title={"Bosagora Wallet"} 
                    navigation={this.props.navigation} 
                    add={true} 
                />
                <View style={{flex:1}}>

                    {this.renderLoader()}
                    <View style={styles.topOuterView}>
                        <View style={styles.head}>
                            <View>
                            <Text style={styles.textHeader}>Total</Text>
                            <Text style={styles.textHeaderLeft}>Spendable</Text>
                        </View>
                            <View style={styles.mgLft}>
                            <Text style={styles.textHeader}>Self Balance</Text>
                            <Text  style={styles.innerTxt}>5000.00000000</Text>
                            <Text  style={styles.innerTxt}>1000.00000000</Text>
                        </View>
                            <View style={styles.mgLft}>
                            <Text style={styles.textHeader}>Total Balance</Text>
                            <Text style={styles.innerTxt}>5000.00000000</Text>
                            <Text style={styles.innerTxt}>1000.00000000</Text>
                        </View>
                        </View>
                        <View style={styles.view1}>
                                <Text style={styles.status}>Mode Status</Text>
                                <Text style={styles.statusWorth}>Sudo 5</Text>
                        </View>
                        <View style={styles.view2}>
                            <Text style={styles.textCounter}>1</Text>
                        </View>
                    </View>

                        
                <ScrollableTabView

                    tabBarUnderlineColor={color.Primery}
                    tabBarUnderlineStyle ={{color:color.Primery}}
                    tabBarBackgroundColor ={'#fff'}
                    tabBarActiveTextColor={color.Primery}
                    tabBarInactiveTextColor={color.graycolor}
                    initialPage={0}
                    renderTabBar={() => 
                    <ScrollableTabBar />}
                    >
                        <Recent  tabLabel="Recent" _messages={messages} {...this.props}   />
                        <Pending tabLabel="Pending" _messages={messages} {...this.props}  />

                    </ScrollableTabView>

                    
            
            



                </View>			
            </View>			

		
		);
	}	

    renderLoader() {
    if (this.props.messages?.loader) {
        return <Loader />;
    }
    }
};

 ///reducer data
 const mapStateToProps = ({messages}) => ({
    messages: messages.get_,
});

const mapDispatchToProps = (dispatch) => ({
  resetPage:()=>dispatch({type: Actions.DRESETPAGE, payload:1}),
  get_Messages: (params=null, navigation=null, _this)=>dispatch(get_Messages(params, navigation, _this )),
});

export default  connect(mapStateToProps,mapDispatchToProps)(Dashboard);
