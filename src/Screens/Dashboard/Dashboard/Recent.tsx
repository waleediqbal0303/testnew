import React, { Component} from "react";
import { View, Image, ImageBackground, StatusBar, Text ,FlatList,TouchableOpacity,Alert} from "react-native";

import {Header,Loader} from '../../../Components';
import { color } from "../../../res";
import styles from "./recentStyles";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import ActionButton from 'react-native-action-button';


class Recent extends Component {

    randerItem=(data)=>{
        let color='';
        let icon='';
        if(data.index==0){
            color= '#38D3AE';
            icon='add';
        }
        else if(data.index==1){
            color='#E74C3C';
            icon='ios-remove-sharp';
        }
        else if(data.index==2){
            color= '#7E75FF';
            icon='md-aperture-outline';
        }
        else{
            color= '#38D3AE';
            icon='add';
        }
        return (
            <View style={styles.view5}>                          
                <View style={[styles.view4, {backgroundColor:color}]}>
                    <Ionicons
                        name={icon}
                        color={'#fff'}
                        size={25}
                        />
                </View>
                <View style={styles.view6}>
                        <Text style={styles.view7} numberOfLines={1}>{data.item.name}</Text>
                        <Text style={styles.view8}  numberOfLines={1}>{data.item.name}</Text>
                </View>
                <View style={styles.view9}>
                        <Text style={styles.view10} numberOfLines={1}>2,748.94 BOA</Text>
                        <Text style={styles.view11}  numberOfLines={1}>Time : 01:20:03</Text>
                </View>
            </View>
            );
        }


    render() {
       let  messages= this.props?._messages;
		return (
			<View style={{flex:1}}>
                <View style={styles.view1}>
                    <Text style={styles.view2}>Recent Transactions</Text>
                    <View style={styles.view3}>
                        <Ionicons
                            name='options'
                            color={color.Primery}
                            size={25}
                            />
                    </View>
                </View>
                
                <FlatList
                    data={messages?._data}
                    renderItem={this.randerItem}
                    style={{marginHorizontal:20, marginTop:0}}
                    showsVerticalScrollIndicator={false}


                    // onEndReached={this.onEndReached.bind(this)}
                    // onEndReachedThreshold={0.5}
                    // onRefresh={() => this._refresh()}
                    // refreshing={this.state.isFetching}
                    // onMomentumScrollBegin={() => { this.setState({onEndReachedCalledDuringMomentum : false})}}
                
                />
                <TouchableOpacity style={styles.view12}>
                    <Ionicons
                        name='repeat'
                        color={color.textPrimary}
                        size={25}
                        />
                </TouchableOpacity>

                
            </View>			

		
		);
	}	

};



export default Recent;
