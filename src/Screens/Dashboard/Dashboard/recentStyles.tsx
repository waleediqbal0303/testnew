import { StyleSheet, Platform } from 'react-native';
import {color, baseStyles} from '../../../res';

export default StyleSheet.create({
    view1:{
        justifyContent:"center", 
        flexDirection:"row", 
        marginTop:10
    },
    view2:{
        fontSize:16, 
        fontWeight:'bold', 
        marginLeft:20
    },
    view3:{
        flex:1, 
        justifyContent:"flex-end", 
        alignItems:"flex-end", 
        marginRight:20
    },
    view4:{
        width:35,
        height:35,
        borderRadius:40,
        backgroundColor:color.Primery,
        justifyContent:"center",
        alignItems:"center"
    },
    view5:{
        height:70, 
        flexDirection:"row", 
        borderBottomColor:'#BDC3C7', 
        borderBottomWidth:0.3, 
        alignItems:"center"
    },
    view6:{
        flex:0.5, 
        marginLeft:10, 
        marginRight:10
    },
    view7:{
        color:color.Primery, 
        fontSize:16
    },
    view8:{
        color:color.graycolor, 
        fontSize:12
    },
    view9:{
        flex:0.5, 
        marginLeft:10, 
        alignItems:"flex-end"
    },
    view10:{
        color:'#000', 
        fontSize:14, 
        fontWeight:"bold"
    },
    view11:{
        color:color.graycolor, 
        fontSize:10
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    view12:{ 
        marginTop:-250, 
        marginRight:20, 
        alignSelf:"flex-end",  
        width:50, 
        height:50, 
        borderRadius:50, 
        backgroundColor:color.Primery, 
        justifyContent:"center", 
        alignItems:"center"
    },
});