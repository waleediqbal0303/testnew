import React from "react";
import {
	View,
	Text,
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import  Dashboard from '../src/Screens/Dashboard/Dashboard/Dashboard';
import  Splash from '../src/Screens/Splash/Splash';
import DrawerContent from '../src/Components/Drawer/Drawer';
import { TouchableOpacity } from "react-native-gesture-handler";

import { Provider } from "react-redux";
import store from "./ActionsReducers/index";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();




function DashBoardRouter() {
	return (
		<Stack.Navigator headerMode="none"> 
			<Stack.Screen name="Dashboard" component={Dashboard}/> 
		</Stack.Navigator>
	);
}



function RootNavigator(){
return (
	<Drawer.Navigator initialRouteName="DashBoardRouter" drawerContent={(props) => <DrawerContent {...props}/>}>
	
		<Drawer.Screen name="DashBoardRouter" component={DashBoardRouter} />
		
	</Drawer.Navigator>
);
};

function MainRouter() {
	return (
		<NavigationContainer>
			<Stack.Navigator headerMode="none">
				<Stack.Screen name="RootNavigator" component={RootNavigator} />
				
			</Stack.Navigator>
		</NavigationContainer>
	);
}



function Top_Router() {
	return (
	  <Provider store={store}>
		<MainRouter/>
	  </Provider>
	);
}

export default Top_Router;
