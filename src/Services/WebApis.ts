import axios from 'axios';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


const BASE_URL=  'http://ec2-3-89-252-137.compute-1.amazonaws.com'; // test link
const API_PARAMETER = '/api/';
const Token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJuc0BybnNzb2wuY29tIiwidXNlcklEIjoiNjAwNTBiYjFlNzc5NGU1NDEwM2I0OTY0IiwidHlwZSI6InN1cGVyLWFkbWluIiwibmFtZSI6IlJOUyBBZG1pbiIsImlhdCI6MTYxNDY4OTMxOSwiZXhwIjoxNjE0Nzc1NzE5fQ.-d7bgfk6nffe_WlSA9MNyNYORv9FFaasOWhuG-DQz7Q';

const BASE_URL_test=  'https://jsonplaceholder.typicode.com'; // test link
const API_PARAMETER_test = '/';

export default class WebApis {

  apiError() {
    Alert.alert(
      'Action Failed',
      'Something went wrong. Check your internet connection try again later!.',
      [{text: 'OK', onPress: () => console.log('action failed')}],
    );
  }




  async post_withToken(url, data) {
    
    return axios.post(url, data, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${Token}`,
      },
    });
  }


  async post_withoutToken(url, data) {
    
    return axios.post(url, data, {
      headers: {
        Accept: 'application/json',
      },
    });
  }


  async get_withoutToken(url) {
    return axios.get(url, {
      headers: {
        Accept: 'application/json',
      },
    });
  }


  getMessages(params=null){ 
    
    let url = BASE_URL_test  + API_PARAMETER_test +'comments';
    if(params!=null){
      url= url+"?id="+params;
    }
    return this.get_withoutToken(url);
  }

}






