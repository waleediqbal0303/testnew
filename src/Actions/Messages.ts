

import { 
    LOADER,
    DASHBOARD,
    DRESETPAGE,
  
} from './Types/ActionTypes';
import WebApis from '../Services/WebApis';
import AsyncStorage from "@react-native-community/async-storage";



const get_Messages = (params=null, navigation=null, _this=null) => async dispatch => {

    try {
        dispatch({ type: LOADER, payload: true});
        let getMessages = await new WebApis().getMessages(params);
        console.log("getMessages", getMessages);

        if (getMessages!=null) {
            let _resp  = getMessages.data;
            dispatch({ type: DASHBOARD, payload: _resp});
        }
      } 
      catch (err) {
          console.log("Err", err);
      }
      dispatch({ type:LOADER, payload: false});

};

export {get_Messages};