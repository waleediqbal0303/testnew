import React from 'react';
import {SafeAreaView, View, Text, Image, TouchableOpacity, StatusBar, Platform} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {color} from '../../res';
import { useState } from 'react';
import styles from './styles';
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;



const Header = props => {
    if(props.screen=='DashBoard')
        return (
                <SafeAreaView
                
                style={styles.view}>
                    {
                    Platform.OS === 'ios' 
                    ? 
                    <StatusBar barStyle="light-content" backgroundColor={color.Primery} /> 
                    : 
                    <StatusBar barStyle="light-content" backgroundColor={color.Primery} />
                    }
                    <View style={styles.mainView}>
                        <TouchableOpacity style={{}} onPress={() => props.navigation.openDrawer()}>
                            <Ionicons
                                name='menu-sharp'
                                color={color.textPrimary}
                                size={25}
                                style={styles.pd_lf}
                                />
                        </TouchableOpacity>
                        <View style={styles.textView}>
                            <Text style={styles.headingTxtBack}>{props.title}</Text>
                        </View>
                        <TouchableOpacity >
                            <Ionicons
                                name='person-add'
                                color={color.textPrimary}
                                size={20}
                                style={styles.pd_right}
                                />
                        </TouchableOpacity>
                        <TouchableOpacity >
                            <Ionicons
                                name='earth'
                                color={color.textPrimary}
                                size={20}
                                style={styles.pd_right}
                                />
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
        )
};

export {Header};
