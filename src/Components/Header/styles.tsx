import { StyleSheet, Platform } from 'react-native';
import {color} from '../../res';

export default StyleSheet.create({

    mainView:{
      height:60,
      flexDirection:"row", 
      backgroundColor: color.Primery,
      alignItems:"center"
    },
    textView:{
      flex:1, 
      marginLeft:20
    },
    view:{
        ...Platform.select({
          ios: {
            
          },
          android: {
            marginTop:0,
          },
        }),
        backgroundColor: color.Primery,
      },
    
      leftImg:{
        height:35,
        width:25,
        marginLeft:20
      },
      headingTxt:{
        fontSize:20, 
        color:color.Primery, 
        marginLeft:-50,
        fontWeight:"bold",
      },
      headingTxtBack:{
        fontSize:16,
        fontWeight:'bold', 
        color:color.textPrimary,
      },
      touchableOpacityRight:{
        height:30, 
        width:30,
        alignItems:"center", 
        marginRight:20, 
        backgroundColor:color.Primery, 
        borderRadius:35
      },
      pd_lf:{ paddingLeft: 15},
      pd_right:{ paddingRight: 15},
      mg_lf40:{paddingLeft:0}
    
    
    
    
     
        // view2:{flex:1, flexDirection:'row', alignItems:'center'},
        // view3:{flex:1,justifyContent: 'center', marginLeft:-37,alignItems: 'center'},
        // view4:{flex:1, flexDirection:'row', alignItems:'center'},
        // view5:{justifyContent: 'center', alignItems: 'center', flexDirection:"row"},
    
    
    
    
    
        // txt1:{fontSize: 18, color: '#fff', fontWeight: 'normal'},
        // txt2:{flex:1,fontSize: 18, color: COLORS.MAIN, fontWeight: 'bold', marginLeft:30},
    
        // icon1:{ paddingRight: 10, marginTop:0},
    
    
    
        // touchable1:{alignItems:"center", justifyContent:"center"},
    
});
