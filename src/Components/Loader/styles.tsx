import { StyleSheet } from 'react-native';
import {color} from '../../res';

export default StyleSheet.create({

    view1:{
        width: '100%',
        height: 69,
        borderRadius: 10,
        backgroundColor: '#fff',
        justifyContent: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 6,
        },
        shadowOpacity: 0.39,
        shadowRadius: 8.3,

        elevation: 13,
      },
    view2:{
        flexDirection: 'row',
        marginVertical: 20,
        borderBottomWidth: 0.5,
        borderBottomColor: '#959595',
        padding: 10,
      },
    

    view3:{width: '15%', justifyContent: 'center', alignItems: 'center'},
    view4:{width: '30%', alignItems: 'center', justifyContent: 'center'},
    view5:{width: '85%'},
    view6:{
        width: '70%',
        justifyContent: 'center',
        alignItems: 'flex-start',
      },










    txtInput1:{
        height: 25,
        borderRadius: 0,
        borderBottomWidth: 1,
        borderBottomColor: color.Primery,
        padding: 5,
        fontSize:14,
        paddingLeft:0,
        marginVertical: 5,
        color: color.Primery,
      },
    txtInput2:{
        borderRadius: 30,
        height: 40,
        opacity: 6,
        backgroundColor: '#E5EEF5',
        paddingLeft: 20,
        color: '#000',
      },

    touchable1:{
        backgroundColor: color.Primery,
        width: '100%',
        marginHorizontal:20,
        elevation: 13,
        alignItems:'center',
        height: 45,
        borderRadius: 10,
        opacity: 0.9
      },
    touchable2:{
        flexDirection: 'row',
        width: 275,
        height: 45,
        borderRadius: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 6,
        },
        shadowOpacity: 0.39,
        shadowRadius: 8.3,
        elevation: 13,
      },
      
      txt1:{
        color: '#fff',
        paddingTop: 10,
        fontWeight:'bold',
        paddingBottom: 7,
        textAlign: 'center',
        fontSize: 14,
      },
      txt2:{fontSize: 20, paddingLeft: 10},
      txt3:{fontWeight: 'bold', fontSize: 12, color: '#000'},
      txt4:{fontSize: 10, color: '#707070'},
      txt5:{fontSize: 8, color: '#9F9F9F'},






      container1:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        zIndex:100
      },

      // container1:{ //normally in center of screen
      //   flex: 1,
      //   justifyContent: 'center',
      //   alignItems: 'center',
      //   position: 'absolute',
      //   left: 0,
      //   right: 0,
      //   top: 0,
      //   bottom: 0,
      // },


});