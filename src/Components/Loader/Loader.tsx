import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import styles from './styles';
import {color} from '../../res';

const Loader = () => {
  return (
    <View style={styles.container1}>
      <ActivityIndicator size="large" color={color.Primery} />
    </View>
  );
};


export {Loader};
