import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import {
  DrawerItem,
  DrawerContentScrollView,
  useIsDrawerOpen,
  
} from '@react-navigation/drawer';
import {
  useTheme,
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  Text,
  TouchableRipple,
  Switch
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function DrawerContent(props) {
    console.log("props", props)
    
  return (
    <View style={styles.drawerContent}>
        <ScrollView>
        <View
            >
            <View style={styles.userInfoSection}>
            <Avatar.Image
                source={{
                uri:
                    'https://pbs.twimg.com/profile_images/952545910990495744/b59hSXUd_400x400.jpg',
                }}
                size={80}
            />
            <Title style={styles.title}>Dawid Urbaniak</Title>

            </View>
            <View style={styles.topView}/>
            </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
    topView:{
        borderBottomWidth:0.3, 
        borderBottomColor:"gray", 
        margin:10, 
        marginTop:30
    },
  drawerContent: {
    flex: 1,
    backgroundColor:"#37474F",
    paddingTop:40
  },
  userInfoSection: {
    alignItems:"center"
  },
  title: {
    marginTop: 20,
    fontWeight: 'bold',
    color:"#fff"
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },

  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  labelStyle:{
      color:'#fff',
  },
});


