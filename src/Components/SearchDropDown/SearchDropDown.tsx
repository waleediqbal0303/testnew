import React, { Component,useEffect, useState } from "react";
import { View, Image, ImageBackground, StatusBar, Text, TextInput, TouchableOpacity, ActivityIndicator } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import styles from './styles';
import { color } from "../../res";

export default class SearchDropDown extends Component {
    constructor(props) {
        super(props);
    }
    onChangeText=(text)=>{

        let _this = this.props?._this;
        let type = this.props?._type;

        if(type=="InventoryModels"){
            if(_this.modelWaiting)
                clearTimeout(_this.modelWaiting);
                _this.modelWaiting = setTimeout(() => {
                _this.modelWaiting = null;
                _this.searchModel(text);
            }, 200);
        }
        if(type=="InventoryBrands"){
            if(_this.brandWaiting)
                clearTimeout(_this.brandWaiting);
                _this.brandWaiting = setTimeout(() => {
                _this.brandWaiting = null;
                _this.searchBrand(text)
            }, 200);
        }
        if(type=="InventoryItems"){
            if(_this.itemWaiting)
                clearTimeout(_this.itemWaiting);
                _this.itemWaiting = setTimeout(() => {
                _this.itemWaiting = null;
                _this.searchItem(text)
            }, 200);
        }

        if(type=="InvoiceModels"){
            if(_this.modelWaiting)
                clearTimeout(_this.modelWaiting);
                _this.modelWaiting = setTimeout(() => {
                _this.modelWaiting = null;
                _this.searchModel(text);
            }, 200);
        }
        if(type=="InvoiceBrands"){
            if(_this.brandWaiting)
                clearTimeout(_this.brandWaiting);
                _this.brandWaiting = setTimeout(() => {
                _this.brandWaiting = null;
                _this.searchBrand(text)
            }, 200);
        }
        if(type=="InvoiceItems"){
            if(_this.itemWaiting)
                clearTimeout(_this.itemWaiting);
                _this.itemWaiting = setTimeout(() => {
                _this.itemWaiting = null;
                _this.searchItem(text)
            }, 200);
        }


        if(type=="ItemModels"){
            if(_this.modelWaiting)
                clearTimeout(_this.modelWaiting);
                _this.modelWaiting = setTimeout(() => {
                _this.modelWaiting = null;
                _this.searchModel(text);
            }, 200);
        }
        if(type=="ItemBrands"){
            if(_this.brandWaiting)
                clearTimeout(_this.brandWaiting);
                _this.brandWaiting = setTimeout(() => {
                _this.brandWaiting = null;
                _this.searchBrand(text)
            }, 200);
        }

        if(type=="ModelBrands"){
            if(_this.brandWaiting)
                clearTimeout(_this.brandWaiting);
                _this.brandWaiting = setTimeout(() => {
                _this.brandWaiting = null;
                _this.searchBrand(text)
            }, 200);
        }

        if(type=="InvoiceCutomsers"){
            if(_this.customerWaiting)
                clearTimeout(_this.customerWaiting);
                _this.customerWaiting = setTimeout(() => {
                _this.customerWaiting = null;
                _this.searchCustomer(text)
            }, 200);
        }


    }
    onPressItem=(data)=>{

        let type = this.props?._type;
        let _this = this.props?._this;

        if(type=="InvoiceCutomsers"){
            _this.selectCustomer(data);
        }

        if(type=="InventoryModels"){
            _this.selectModel(data);
        }
        if(type=="InventoryBrands"){
            _this.selectBrand(data);
        }
        if(type=="InventoryItems"){
            _this.selectItem(data);
        }


        if(type=="InvoiceModels"){
            _this.selectModel(data);
        }
        if(type=="InvoiceBrands"){
            _this.selectBrand(data);
        }
        if(type=="InvoiceItems"){
            _this.selectItem(data);
        }

        if(type=="ItemModels"){
            _this.selectModel(data);
        }
        if(type=="ItemBrands"){
            _this.selectBrand(data);
        }

        if(type=="ModelBrands"){
            _this.selectBrand(data);
        }


    }
    render() {
        let _props = this.props;
        console.log("_props", _props);
        let type = this.props?._type;
            return (
                <View>
                    <View style={styles.searchBox}>
                        <TextInput 
                            style={styles.searchInput} 
                            placeholder= {_props?.title} 
                            placeholderTextColor={color.graycolor} 
                            onChangeText={(text)=>{this.onChangeText(text)}}
                        />
                        {_props?.loader&&
                            <View style={styles.pop_loader}> 
                                <ActivityIndicator size="small" color={color.Primery} />
                            </View>
                        }
                    </View>
                    <ScrollView style={styles.scroll} >
                    <View>
                        {_props?.data?.map(item=>{
                            return(
                                <TouchableOpacity 
                                    style={styles.dropDownItem}
                                    onPress={()=>{
                                        this.onPressItem(item)
                                    }}
                                    >
                                    {type=="ModelBrands"&&
                                        <Text style={styles.dropItemTxt}>{item?.brand_name}</Text>
                                    }
                                    
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                </ScrollView>
                </View>
            );
    }
}
