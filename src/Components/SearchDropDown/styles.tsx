import { StyleSheet, Platform } from 'react-native';
import {color, baseStyles} from '../../res';

export default StyleSheet.create({

    mainView:{
      flex:1, 
      flexDirection:"row", 
      backgroundColor:color.Primery, 
      alignItems:"center"
    }
    ,
    TextInput:{
      borderBottomColor:color.graycolor,
      borderBottomWidth:0.5,
      marginTop:20,
      width:'100%',
      fontFamily:baseStyles.fontType.fontFamily
      
    },
    errorStyle:{
      marginTop:5, 
      color:color.Primery,
      alignSelf:"flex-start",
      marginBottom:0,
      fontSize:12,
      fontFamily:baseStyles.fontType.fontFamily
  },
  Addbutton:{
    color:'white',
    backgroundColor:color.Primery,
    paddingVertical:10,
    paddingHorizontal:20,
    borderRadius:5,
    fontFamily:baseStyles.fontType.fontFamily
  },
  cancelButton:{
   color:'white',
  
   backgroundColor:color.Secondry,
   paddingVertical:10,
   paddingHorizontal:25,
    borderRadius:5
 },
 lowerview:{
  marginHorizontal:30,
  marginTop:20,
  marginBottom:10,
  alignSelf:'flex-end',
   flexDirection:'row',
   color:'#707070',

},
dropDownItem:{
  height:40,
  width:'100%', 
  justifyContent:"center",
  backgroundColor:color.graycolor,
  borderColor:color.textPrimary,
  borderBottomWidth:0.5


},
dropItemTxt:{
  paddingLeft:5,
  fontSize:12,
  color:color.textPrimary
},
topView:{
  marginTop:40, 
  justifyContent:"center", 
  marginHorizontal:30
},
secView:{
  width:'100%', 
  flexDirection:"row", 
  justifyContent:"center", 
  alignItems:"center"
},
touchView: {
  marginLeft:-40, 
  marginBottom:-25, 
  justifyContent:"center", 
  alignItems:"center"
},
scroll:{
  margin:10, 
  maxHeight:200
},
secTopView:{
  justifyContent:"center", 
  marginHorizontal:30
},
fieldView:{
  flexDirection:"row", 
  justifyContent:"center"
},
innerHalfView:{
  flex:0.5, 
  justifyContent:"center", 
  alignItems:"center", 
  marginHorizontal:30
},
viewGeneral:{
  justifyContent:"center", 
  alignItems:"center", 
  marginHorizontal:30
},
searchBox:{
  height:40, 
  width:'95%', 
  marginHorizontal:10, 
  marginTop:10, 
  borderColor:color.graycolor, 
  borderWidth:2,
  alignItems:"center",
  flexDirection:'row'
},
searchInput:{
  flex:1,
  fontFamily:baseStyles.fontType.fontFamily,
  color:color.graycolor
},
pop_loader:{
  marginHorizontal:10
},



    
});
