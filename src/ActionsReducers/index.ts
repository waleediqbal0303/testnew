import { combineReducers, createStore,applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import MESSAGES from './Messages';

const AppReducers = combineReducers({
    messages:MESSAGES
});

const rootReducer = (state, action) => {
	return AppReducers(state,action);
}

export default createStore(
  rootReducer,
  applyMiddleware(thunk)
);
