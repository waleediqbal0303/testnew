const static_text = {
  
    //texts of sign in screen

   
    BrandName:'Brand Name',
    ModelName:'Model Name',
    AddBrandButton:'Add Brand',
    CancelButton:'Cancel',
    AddModelButton:'Add Model',
    ModelNameItem:'Model Name Item',
   
    //..........................................
  
    si_btn_signIn:'LOG IN',
    si_txt_textinput:'User Name',
    si_txt_textinput2:'Password',
    si_txt_forgotpassword:'Forgot Password?',
    si_txt_dnthavetimeaccount:"Don't have an account?",
    si_txt_skip:'Skip',

    //................................................

    customerName:'Customer Name',
    companyName:'Company Name',
    customerMobile:'Customer Mobile',
    cancelCustomer:'Cancel',


  };
  
  export {static_text};